#!/bin/bash
set -euxo pipefail

image="${CI_REGISTRY_IMAGE}"
tag="${CI_COMMIT_REF_SLUG}"

echo "Building $image:$tag"

ctr=$(buildah from gitlab.com:443/s18k-container/dependency_proxy/containers/fedora:latest)

buildah run $ctr dnf update -y

buildah commit --squash --rm $ctr "$image:$tag"